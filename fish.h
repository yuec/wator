#ifndef FISH_H
#define FISH_H

#include <stdint.h>
//#include "wator.h"

struct _fish
{
	uint32_t age;
	uint32_t coord_x;
	uint32_t coord_y;
	uint32_t move;
};

typedef struct _fish fish;

void fish_init(fish *, uint32_t, uint32_t);
void fish_set(fish *, fish *);
void set_fish(fish *, uint32_t, uint32_t);

#endif
