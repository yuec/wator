/* Copyright (C) 
 * 2012 - Yue Cheng
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#include "shark.h"

void shark_init(shark *s, uint32_t x, uint32_t y)
{
	s->age = 0;
	s->starv = 0;
	s->coord_x = x;
	s->coord_y = y;
	s->move = 0;

	return;
}

void shark_set(shark *s0, shark *s1)
{
	s0->age = s1->age;
	s0->starv = s1->starv;
	s0->coord_x = s1->coord_x;
	s0->coord_y = s1->coord_y;
	s0->move = s1->move;

	return;
}

void set_shark(shark *s, uint32_t x, uint32_t y)
{
	(s->age)++;
	s->starv++;
	s->coord_x = x;
	s->coord_y = y;
	s->move = 1;

	return;
}

void clear_shark_starv(shark *s)
{
	s->starv = 0;
	return;
}
