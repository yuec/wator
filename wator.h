/* Copyright (C) 
 * 2012 - Yue Cheng
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#ifndef WATOR_H
#define WATOR_H

#include <stdint.h>
#include <omp.h>
#include "fish.h"
#include "shark.h"

#define DEBUG

#define N		16	// grid dimension
#define FB		20	// fish breeding age: the breeding of fish is cyclic
#define FO		55	// old age when fish dies
#define SB		30	// shark breeding age: the breeding of shark is cyclic
#define SS		7	// starvation age of shark
#define SO		59	// old age when shark dies

#define N_FISH	100
#define N_SHARK	10
#define TERM	10000

#define GET_IDX_X(a, b)		((a) / (b))
#define GET_X_MIN(a, b, c)	((GET_IDX_X(a,b)) * c)
#define GET_X_MAX(a, b, c)	(((GET_IDX_X(a,b))+1)*c - 1)
#define GET_IDX_Y(a, b)		((a) % (b))
#define GET_Y_MIN(a, b, c)	((GET_IDX_Y(a,b)) * c)
#define GET_Y_MAX(a, b, c)	(((GET_IDX_Y(a,b))+1)*c - 1)
#define GLOBAL_X(a, b, c)	((c) + (a)/(b))
#define GLOBAL_Y(a, b, c)	((c) + (a)%(b))
#define WRAP_DOWN(a)		(((a)+1) == 0) ? (N-1) : (a)
#define WRAP_UP(a)			(((a)-1) == (N-1)) ? 0 : (a)
#define WRAP_RIGHT(a)		WRAP_DOWN(a)
#define WRAP_LEFT(a)		WRAP_UP(a)


enum _item {
	EMPTY,
	FISH,
	SHARK
};

typedef enum _item item;

struct _cell
{
	item state;
	fish *f;
	shark *s;
};

typedef struct _cell cell;

void grid_init(cell [][N], uint32_t);
void lock_init(omp_lock_t [][N], omp_lock_t *, omp_lock_t *);
void move(cell [][N], omp_lock_t [][N], uint32_t, uint32_t, uint32_t, uint32_t, 
			uint32_t, uint32_t *, uint32_t *, omp_lock_t *, omp_lock_t *);
void clear_move(cell [][N]);
void print_info(cell [][N], uint32_t, uint32_t, uint32_t);

#endif
