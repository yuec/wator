/* Copyright (C) 
 * 2012 - Yue Cheng
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <omp.h>
#include "wator.h"

static inline void insert_fish(cell ocean[][N], uint32_t x, uint32_t y, fish *f)
{
	ocean[x][y].state = FISH;
	ocean[x][y].f = (fish *)malloc(sizeof(fish));
	ocean[x][y].f->age = f->age;
	ocean[x][y].f->coord_x = f->coord_x;
	ocean[x][y].f->coord_y = f->coord_y;
	return;
}

static inline void remove_fish(cell ocean[][N], uint32_t x, uint32_t y)
{
	if (ocean[x][y].state == FISH) {
		ocean[x][y].f = NULL;
		ocean[x][y].s = NULL;
		ocean[x][y].state = EMPTY;
	}
	return;
}

static inline void insert_shark(cell ocean[][N], uint32_t x, uint32_t y, shark *s)
{
	ocean[x][y].state = SHARK;
	ocean[x][y].s = (shark *)malloc(sizeof(shark));
	ocean[x][y].s->age = s->age;
	ocean[x][y].s->coord_x = s->coord_x;
	ocean[x][y].s->coord_y = s->coord_y;
	ocean[x][y].s->starv = s->starv;
	return;
}

static inline void remove_shark(cell ocean[][N], uint32_t x, uint32_t y)
{
	if (ocean[x][y].state == SHARK) {
		ocean[x][y].s = NULL;
		ocean[x][y].f = NULL;
		ocean[x][y].state = EMPTY;
	}
	return;
}

/**
 * @brief initialization of the ocean grid
 *
 * @param ocean[][N]
 * @param nthreads
 */
void grid_init(cell ocean[][N], uint32_t nthreads)
{
	for (uint32_t i = 0; i < N; i++)	{
		for (uint32_t j = 0; j < N; j++) {
			memset(&ocean[i][j], 0, sizeof(cell));
			ocean[i][j].state = EMPTY;
			ocean[i][j].f = NULL;
			ocean[i][j].s = NULL;
		}
	}

	uint32_t x, y;
	for (uint32_t i = 0; i < N_FISH; i++) {
pos1:	x = rand() % N;
		y = rand() % N;
		if (ocean[x][y].state == EMPTY) {
			fish f;
			fish_init(&f, x, y);
			insert_fish(ocean, x, y, &f);
		} else	goto pos1;
	}

	for (uint32_t i = 0; i < N_SHARK; i++) {
pos2:	x = rand() % N;
		y = rand() % N;
		if (ocean[x][y].state == EMPTY) {
			shark s;
			shark_init(&s, x, y);
			insert_shark(ocean, x, y, &s);
		} else	goto pos2;
	}

	return;
}

/**
 * @brief initialization of the locks
 *
 * @param celllock[][N]
 * @param fishupdate_lock
 * @param sharkupdate_lock
 */
void lock_init(omp_lock_t celllock[][N], omp_lock_t *fishupdate_lock, 
			omp_lock_t *sharkupdate_lock)
{
	for (uint32_t i = 0; i < N; i++) {
		for (uint32_t j = 0; j < N; j++) {
		    omp_init_lock(&celllock[i][j]);
		}
	}
#if 0
	*fishupdate_lock = (omp_lock_t *)malloc(sizeof(omp_lock_t));
	*sharkupdate_lock = (omp_lock_t *)malloc(sizeof(omp_lock_t));
#endif
//	omp_init_lock(fishupdate_lock);
	omp_init_lock(sharkupdate_lock);
	return;
}

#if 0
void lock_init(omp_lock_t ***celllock, omp_lock_t *fishupdate_lock, 
			omp_lock_t *sharkupdate_lock)
{
	*celllock = (omp_lock_t **)malloc(N*sizeof(omp_lock_t *));
	for (uint32_t i = 0; i < N; i++) {
		(*celllock)[i] = (omp_lock_t *)malloc(N*sizeof(omp_lock_t));
		memset((*celllock)[i], 0, N*sizeof(omp_lock_t));
		for (uint32_t j = 0; j < N; j++) {
		    omp_init_lock(&((*celllock)[i][j]));
			if (omp_test_lock(&((*celllock)[i][j])) != 0) {
				fprintf(stderr, "lock corrupted\n");
//				exit(1);
			}
		}
	}
#if 0
	*fishupdate_lock = (omp_lock_t *)malloc(sizeof(omp_lock_t));
	*sharkupdate_lock = (omp_lock_t *)malloc(sizeof(omp_lock_t));
#endif
//	omp_init_lock(fishupdate_lock);
	omp_init_lock(sharkupdate_lock);
	return;
}
#endif

/**
 * @brief move activity of fish
 *
 * @param ocean[][N]
 * @param x
 * @param y
 * @param path_x
 * @param path_y
 */
static void fish_go(cell ocean[][N], uint32_t x, uint32_t y, uint32_t path_x,
			uint32_t path_y)
{
	fish f;
	fish_set(&f, ocean[x][y].f);
	insert_fish(ocean, path_x, path_y, &f);
	set_fish(ocean[path_x][path_y].f, path_x, path_y);
	remove_fish(ocean, x, y);

	return;
}

/**
 * @brief breed activity of fish
 *
 * @param ocean[][N]
 * @param x
 * @param y
 * @param nfish
 * @param fishupdate_lock
 */
static void fish_breed(cell ocean[][N], uint32_t x, uint32_t y, uint32_t *nfish,
			omp_lock_t *fishupdate_lock)
{
	fish f;
	fish_init(&f, x, y);
	insert_fish(ocean, x, y, &f);

#pragma omp critical
	(*nfish)++;

	return;
}

/**
 * @brief die activity of fish
 *
 * @param ocean[][N]
 * @param x
 * @param y
 * @param nfish
 * @param fishupdate_lock
 */
static void fish_die(cell ocean[][N], uint32_t x, uint32_t y, uint32_t *nfish,
			omp_lock_t *fishupdate_lock)
{
	remove_fish(ocean, x, y);
	if (*nfish == 0)	return;

#pragma omp critical
	(*nfish)--;

	return;
}

/**
 * @brief go activity of shark
 *
 * @param ocean[][N]
 * @param x
 * @param y
 * @param path_x
 * @param path_y
 */
static void shark_go(cell ocean[][N], uint32_t x, uint32_t y, uint32_t path_x,
			uint32_t path_y)
{
	shark s;
	shark_set(&s, ocean[x][y].s);
	insert_shark(ocean, path_x, path_y, &s);
	set_shark(ocean[path_x][path_y].s, path_x, path_y);
	remove_shark(ocean, x, y);

	return;
}

/**
 * @brief eat activity of shark
 *
 * @param ocean[][N]
 * @param x
 * @param y
 * @param path_x
 * @param path_y
 * @param nfish
 * @param fishupdate_lock
 */
static void shark_eat(cell ocean[][N], uint32_t x, uint32_t y, uint32_t path_x,
			uint32_t path_y, uint32_t *nfish, omp_lock_t *fishupdate_lock)
{
	remove_fish(ocean, path_x, path_y);
	clear_shark_starv(ocean[x][y].s);

	if (*nfish == 0)	return;

#if 0
	uint32_t status;
	if ((status = omp_test_lock(fishupdate_lock)) == 0)	
		printf("fishupdate lock unset\n");
#endif
//	omp_set_lock(fishupdate_lock);
#pragma omp critical
	{
		(*nfish)--;
	}
//	omp_unset_lock(fishupdate_lock);

	return;
}

/**
 * @brief breed activity of shark
 *
 * @param ocean[][N]
 * @param x
 * @param y
 * @param nshark
 * @param sharkupdate_lock
 */
static void shark_breed(cell ocean[][N], uint32_t x, uint32_t y, uint32_t *nshark,
			omp_lock_t *sharkupdate_lock)
{
	shark s;
	shark_init(&s, x, y);
	insert_shark(ocean, x, y, &s);

//	omp_set_lock(sharkupdate_lock);
#pragma omp critical
	(*nshark)++;
//	omp_unset_lock(sharkupdate_lock);

	return;
}

/**
 * @brief die activity of shark
 *
 * @param ocean[][N]
 * @param x
 * @param y
 * @param nshark
 * @param sharkupdate_lock
 */
static void shark_die(cell ocean[][N], uint32_t x, uint32_t y, uint32_t *nshark,
			omp_lock_t *sharkupdate_lock)
{
	remove_shark(ocean, x, y);
	if (*nshark == 0)	return;
	
#pragma omp critical
	(*nshark)--;

	return;
}

/**
 * @brief fish randomly pick up a path to go
 *
 * @param path_x
 * @param path_y
 * @param x
 * @param y
 */
static void fish_lookup_path(uint32_t *path_x, uint32_t *path_y, uint32_t x,
			uint32_t y)
{
	while (1) {
		uint32_t m = rand() % 3;
		switch (m) {
			case 0:
				*path_x = WRAP_DOWN(x - 1);
				break;
			case 1:
				*path_x = x;
				break;
			case 2:
				*path_x = WRAP_UP(x + 1);
				break;
		}

		uint32_t n = rand() % 3;
		switch (n) {
			case 0:
				*path_y = WRAP_RIGHT(y - 1);
				break;
			case 1:
				*path_y = y;
				break;
			case 2:
				*path_y = WRAP_LEFT(y + 1);
				break;
		}
		if (*path_x == x && *path_y == y)	continue;
		break;
	}

	return;
}

/**
 * @brief shark picks up a fish to eat randomly,
 * if there is no fish around just select a path to go
 *
 * @param path_x
 * @param path_y
 * @param x
 * @param y
 * @param ocean[][N]
 */
static void shark_lookup_path(uint32_t *path_x, uint32_t *path_y, uint32_t x,
			uint32_t y, cell ocean[][N])
{
	uint32_t xmin = WRAP_DOWN(x - 1);
	uint32_t ymin = WRAP_RIGHT(y - 1);
	uint32_t x_cur, y_cur, n_fish = 0;
	uint32_t rec[9];
	memset(rec, 0, sizeof(rec));
	for (uint32_t i = 0; i < 9; i++) {
		if (i == 4)	continue;
		x_cur = GLOBAL_X(i, 3, xmin) % N;
		y_cur = GLOBAL_Y(i, 3, ymin) % N;
		if (ocean[x_cur][y_cur].state == FISH) {
			n_fish++;
			rec[i] = 1;
		}
	}
	if (n_fish != 0) {
		uint32_t f[n_fish], j = 0;
		for (uint32_t i = 0; i < 9; i++) {
			if (rec[i] != 0)	f[j++] = i;
		}
		uint32_t r = rand() % n_fish;
		*path_x = GLOBAL_X(f[r], 3, xmin) % N;
		*path_y = GLOBAL_Y(f[r], 3, ymin) % N;
		return;
	} else {
		fish_lookup_path(path_x, path_y, x, y);
		return;
	}
}

/**
 * @brief move function including all events for fishes and sharks
 *
 * @param ocean[][N]
 * @param celllock[][N]
 * @param age
 * @param tid
 * @param nthreads
 * @param dim_grid
 * @param dim_block
 * @param nfish
 * @param nshark
 * @param fishupdate_lock
 * @param sharkupdate_lock
 */
void move(cell ocean[][N], omp_lock_t celllock[][N], uint32_t age,
			uint32_t tid, uint32_t nthreads,
			uint32_t dim_grid, uint32_t dim_block, uint32_t *nfish, 
			uint32_t *nshark, omp_lock_t *fishupdate_lock,
			omp_lock_t *sharkupdate_lock)
{
	uint32_t ncells, xmin, xmax, ymin, ymax;
	uint32_t x, y, status;
	uint32_t path_x, path_y;
	ncells = N*N / nthreads;
	xmin = GET_X_MIN(tid, dim_grid, dim_block);
	xmax = GET_X_MAX(tid, dim_grid, dim_block);
	ymin = GET_Y_MIN(tid, dim_grid, dim_block);
	ymax = GET_Y_MAX(tid, dim_grid, dim_block);

	for (uint32_t i = 0; i < ncells; i++) {
		x = GLOBAL_X(i, dim_block, xmin);
		y = GLOBAL_Y(i, dim_block, ymin);
		assert(x>=xmin);
		assert(y>=ymin);
		assert(x<=xmax);
		assert(y<=ymax);
	//	printf("%u %u %u: %u\n", i, dim_block, xmin, x);
	//	printf("%u %u %u: %u\n", i, dim_block, ymin, y);
		omp_set_lock(&celllock[x][y]);
		switch (ocean[x][y].state) {
			case FISH:
				if (ocean[x][y].f->move) {
					omp_unset_lock(&celllock[x][y]);
					break;
				}
				if (ocean[x][y].f->age == FO) {
					fish_die(ocean, x, y, nfish, fishupdate_lock);
					omp_unset_lock(&celllock[x][y]);
					break;
				}
				fish_lookup_path(&path_x, &path_y, x, y);

				/* if the next position is not available then fish won't
				   move so as to avoid deadlock */
//				if ((path_x<xmin)||(path_y<ymin)||(path_x>xmax)||(path_y>ymax)) {
//					if (omp_test_lock(&celllock[path_x][path_y]) == 0) {
				if (((path_x==(N-1))&&(xmin==0))||((path_x==0)&&(xmax==(N-1)))
							||((path_x<xmin)&&(xmin!=0))||((path_x>xmax)&&(xmax!=(N-1)))
							||((path_y==(N-1))&&(ymin==0))||((path_y==0)&&(ymax==(N-1)))
							||((path_y<ymin)&&(ymin!=0))||((path_y>ymax)&&(ymax!=(N-1)))) {
					if (omp_test_lock(&celllock[path_x][path_y]) == 0) {
						set_fish(ocean[x][y].f, x, y);
						omp_unset_lock(&celllock[x][y]);
						//status = omp_test_lock(&celllock[x][y]);
						//assert(status==0);
					} else {
						if (ocean[path_x][path_y].state == EMPTY) {
							fish_go(ocean, x, y, path_x, path_y);
							if ((ocean[path_x][path_y].f->age-1)%FB == 0 &&
										ocean[path_x][path_y].f->age-1 != 0) {
								fish_breed(ocean, x, y, nfish, fishupdate_lock);
								set_fish(ocean[x][y].f, x, y);
							}
							omp_unset_lock(&celllock[path_x][path_y]);
							omp_unset_lock(&celllock[x][y]);
							break;
						}
						set_fish(ocean[x][y].f, x, y);
						omp_unset_lock(&celllock[path_x][path_y]);
						omp_unset_lock(&celllock[x][y]);
					}
					goto out0;
					break;
				}
				omp_set_lock(&celllock[path_x][path_y]);
				if (ocean[path_x][path_y].state == EMPTY) {
					fish_go(ocean, x, y, path_x, path_y);
					if ((ocean[path_x][path_y].f->age-1)%FB == 0 &&
								ocean[path_x][path_y].f->age-1 != 0) {
						fish_breed(ocean, x, y, nfish, fishupdate_lock);
						set_fish(ocean[x][y].f, x, y);
					}
					omp_unset_lock(&celllock[path_x][path_y]);
					omp_unset_lock(&celllock[x][y]);
					break;
				}
				set_fish(ocean[x][y].f, x, y);
				omp_unset_lock(&celllock[path_x][path_y]);
				omp_unset_lock(&celllock[x][y]);
out0:			break;
			case SHARK:
				if (ocean[x][y].s->move) {
					omp_unset_lock(&celllock[x][y]);
					break;
				}
				if (ocean[x][y].s->age == SO) {
					shark_die(ocean, x, y, nshark, sharkupdate_lock);
					omp_unset_lock(&celllock[x][y]);
					break;
				}
				if (ocean[x][y].s->starv >= SS) {
					shark_die(ocean, x, y, nshark, sharkupdate_lock);
					omp_unset_lock(&celllock[x][y]);
					break;
				}
				shark_lookup_path(&path_x, &path_y, x, y, ocean);
				
				/* if the next position is not available then shark won't
				   move so as to avoid deadlock */
//				if ((path_x<xmin)||(path_y<ymin)||(path_x>xmax)||(path_y>ymax)
//							||(path_x)) {
				if (((path_x==(N-1))&&(xmin==0))||((path_x==0)&&(xmax==(N-1)))
							||((path_x<xmin)&&(xmin!=0))||((path_x>xmax)&&(xmax!=(N-1)))
							||((path_y==(N-1))&&(ymin==0))||((path_y==0)&&(ymax==(N-1)))
							||((path_y<ymin)&&(ymin!=0))||((path_y>ymax)&&(ymax!=(N-1)))) {
					if (omp_test_lock(&celllock[path_x][path_y]) == 0) {
						set_shark(ocean[x][y].s, x, y);
						omp_unset_lock(&celllock[x][y]);
//						status = omp_test_lock(&celllock[x][y]);
//						assert(status==0);
						break;
					} else {
						if (ocean[path_x][path_y].state == EMPTY) {
							shark_go(ocean, x, y, path_x, path_y);
							if ((ocean[path_x][path_y].s->age-1)%SB == 0 &&
										ocean[path_x][path_y].s->age-1 != 0) {
								shark_breed(ocean, x, y, nshark, sharkupdate_lock);
								set_shark(ocean[x][y].s, x, y);
							}
							omp_unset_lock(&celllock[path_x][path_y]);
							omp_unset_lock(&celllock[x][y]);
							break;
						} else if (ocean[path_x][path_y].state == FISH) {
							shark_eat(ocean, x, y, path_x, path_y, nfish,
										fishupdate_lock);
							shark_go(ocean, x, y, path_x, path_y);
							clear_shark_starv(ocean[path_x][path_y].s);
							if ((ocean[path_x][path_y].s->age-1)%SB == 0 &&
										ocean[path_x][path_y].s->age-1 != 0) {
								shark_breed(ocean, x, y, nshark, sharkupdate_lock);
								set_shark(ocean[x][y].s, x, y);
							}
							omp_unset_lock(&celllock[path_x][path_y]);
							omp_unset_lock(&celllock[x][y]);
							break;
						} else if (ocean[path_x][path_y].state == SHARK) {
							set_shark(ocean[x][y].s, x, y);
							omp_unset_lock(&celllock[path_x][path_y]);
							omp_unset_lock(&celllock[x][y]);
							break;
						}
					}
					goto out;
					break;
				}
				omp_set_lock(&celllock[path_x][path_y]);
				if (ocean[path_x][path_y].state == EMPTY) {
					shark_go(ocean, x, y, path_x, path_y);
					if ((ocean[path_x][path_y].s->age-1)%SB == 0 &&
								ocean[path_x][path_y].s->age-1 != 0) {
						shark_breed(ocean, x, y, nshark, sharkupdate_lock);
						set_shark(ocean[x][y].s, x, y);
					}
					omp_unset_lock(&celllock[path_x][path_y]);
					omp_unset_lock(&celllock[x][y]);
					break;
				} else if (ocean[path_x][path_y].state == FISH) {
					shark_eat(ocean, x, y, path_x, path_y, nfish,
								fishupdate_lock);
					shark_go(ocean, x, y, path_x, path_y);
					clear_shark_starv(ocean[path_x][path_y].s);
					if ((ocean[path_x][path_y].s->age-1)%SB == 0 &&
								ocean[path_x][path_y].s->age-1 != 0) {
						shark_breed(ocean, x, y, nshark, sharkupdate_lock);
						set_shark(ocean[x][y].s, x, y);
					}
					omp_unset_lock(&celllock[path_x][path_y]);
					omp_unset_lock(&celllock[x][y]);
out:				break;
				}
				set_shark(ocean[x][y].s, x, y);
				omp_unset_lock(&celllock[path_x][path_y]);
				omp_unset_lock(&celllock[x][y]);
				break;
			case EMPTY:
				omp_unset_lock(&celllock[x][y]);
				break;
			default:
				omp_unset_lock(&celllock[x][y]);
				break;
		}
	}
}

/**
 * @brief clear the move flag for fish and shark 
 * to avoid moving multiple steps in each iteration
 *
 * @param ocean[][N]
 */
void clear_move(cell ocean[][N])
{
	for (uint32_t i = 0; i < N; i++) {
		for (uint32_t j = 0; j < N; j++) {
			switch (ocean[i][j].state) {
				case FISH:
					ocean[i][j].f->move = 0;
					break;
				case SHARK:
					ocean[i][j].s->move = 0;
					break;
				default:
					break;
			}
		}
	}

	return;
}

/**
 * @brief print helper function
 *
 * @param ocean[][N]
 * @param week
 * @param nfish
 * @param nshark
 */
void print_info(cell ocean[][N], uint32_t week, uint32_t nfish, uint32_t nshark)
{
	fprintf(stdout, "-------------------------------------------------------\n");
	if (week == 0)	fprintf(stdout, "initial state of the OCEAN\n");
	fprintf(stdout, "week %u: #fish: %u #sharks: %u\n", week, nfish, nshark);
	for (uint32_t i = 0; i < N; i++) {
		for (uint32_t j = 0; j < N; j++) {
			switch (ocean[i][j].state) {
				case FISH:
					fprintf(stdout, "cell[%u][%u]: fish: age: %u"
								" coord_x: %u: coord_y: %u",
								i, j, ocean[i][j].f->age,
								ocean[i][j].f->coord_x, ocean[i][j].f->coord_y);
					if (ocean[i][j].f->age % FB == 0 && 
								ocean[i][j].f->age != 0)
						fprintf(stdout, ", about to breed fish baby");
					if (ocean[i][j].f->age == FO)
						fprintf(stdout, ", about to die");
					fprintf(stdout, "\n");
					break;
				case SHARK:
					fprintf(stdout, "cell[%u][%u]: shark: age: %u: "
								"coord_x: %u: coord_y: %u "
								"starv: %u", 
								i, j, ocean[i][j].s->age, 
								i, j, ocean[i][j].s->starv);
					if (ocean[i][j].s->age % SB == 0 && 
								ocean[i][j].s->age != 0)
						fprintf(stdout, ", about to breed shark baby");
					if (ocean[i][j].s->starv == 0 && week != 0)
						fprintf(stdout, ", just eating the fish in this cell");
					if (ocean[i][j].s->age == SO)
						fprintf(stdout, ", about to die");
					fprintf(stdout, "\n");
					break;
				case EMPTY:
					fprintf(stdout, "cell[%u][%u]: empty\n", i, j);
					break;
				default:
					break;
			}
		}
	}
	fprintf(stdout, "-------------------------------------------------------\n");
	return;
}
