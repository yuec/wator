/* Copyright (C) 
 * 2012 - Yue Cheng
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "wator.h"
#include "fish.h"
#include "shark.h"
#include "utils.h"

uint32_t week = 0;
cell ocean[N][N];
uint32_t nfish = N_FISH;
uint32_t nshark = N_SHARK;
//omp_lock_t celllock[N][N];

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "usage: ./sim [num_threads]\n");
		return EXIT_FAILURE;
	}
	uint32_t nthreads = atoi(argv[1]);
	if (nthreads <= 0) {
		fprintf(stderr, "usage: #threads %d should be greater than 0\n",
					nthreads);
		return EXIT_FAILURE;
	}
	omp_lock_t fishlock;
	omp_lock_t sharklock;
	omp_lock_t celllock[N][N];

	omp_set_num_threads(nthreads);
	memset(ocean, 0, sizeof(ocean));
	memset(celllock, 0, sizeof(celllock));
#pragma omp flush (celllock)
	grid_init(ocean, nthreads);
	lock_init(celllock, &fishlock, &sharklock);
	
	uint32_t c;
	uint32_t tid, dim_grid, dim_block;
	dim_grid = sqrt(nthreads);
	dim_block = N / sqrt(nthreads);

	struct timeval start;
	starttimer(&start);
	/**
	 * openmp section
	 * print out a snapshot before the next iteration
	 *
	 */
#pragma omp parallel \
	shared(ocean, celllock, fishlock, sharklock, nthreads, \
				dim_grid, dim_block) \
	private(tid, week)
	{
		tid = omp_get_thread_num();
#pragma omp barrier
		for (week = 0; week < TERM; week++) {
#ifdef DEBUG
#pragma omp master
			{
				print_info(ocean, week, nfish, nshark);
			}
#endif
#pragma omp barrier
			move(ocean, celllock, week+1, tid, nthreads, dim_grid, dim_block, &nfish, 
						&nshark, &fishlock, &sharklock);
#pragma omp barrier
			{
				if (nfish == 0 && nshark == 0)	break;	
			}
#pragma omp master
			{
				clear_move(ocean);
#ifdef DEBUG
				fprintf(stdout, "type <ENTER> to go to week %u's snapshot...\n",
							week+1);
				c = getchar();
#endif
			}
		}
#pragma omp barrier
#pragma omp master
		{
			if (week < TERM)
				week++;
			endtimer(&start);
#ifdef DEBUG
			print_info(ocean, week, nfish, nshark);
#endif
		}
	}
	fprintf(stdout, "terminate...\n");

	return 0;
}
