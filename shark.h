/* Copyright (C) 
 * 2012 - Yue Cheng
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#ifndef SHARK_H
#define SHARK_H

#include <stdint.h>

struct _shark
{
	uint32_t age;
	uint32_t starv;
	uint32_t coord_x;
	uint32_t coord_y;
	uint32_t move;
};

typedef struct _shark shark;

void shark_init(shark *, uint32_t, uint32_t);
void shark_set(shark *, shark *);
void set_shark(shark *, uint32_t, uint32_t);
void clear_shark_starv(shark *);

#endif
