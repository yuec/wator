/* Copyright (C) 
 * 2012 - Yue Cheng
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
#include "fish.h"
#include "wator.h"

void fish_init(fish *f, uint32_t x, uint32_t y)
{
	f->age = 0;
	f->coord_x = x;
	f->coord_y = y;
	f->move = 0;

	return;
}

void fish_set(fish *f0, fish *f1)
{
	f0->age = f1->age;
	f0->coord_x = f1->coord_x;
	f0->coord_y = f1->coord_y;
	return;
}

void set_fish(fish *f, uint32_t x, uint32_t y)
{
	f->coord_x = x;
	f->coord_y = y;
	(f->age)++;
	f->move = 1;

	return;
}
