/* Copyright (C) 
 * 2012 - Yue Cheng
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include <stdio.h>
#include "utils.h"

void starttimer(struct timeval *start_tv)
{
	gettimeofday(start_tv, NULL);

	return;
}

void endtimer(struct timeval *start_tv)
{
	struct timeval end_tv;
	gettimeofday(&end_tv, NULL);

	double sec = end_tv.tv_sec - start_tv->tv_sec;
	double microsec = end_tv.tv_usec - start_tv->tv_usec;
	double diff = sec*1000000 + microsec;

	fprintf(stdout, "    time\n");
	fprintf(stdout, "--------------------\n");
	fprintf(stdout, "%13.4f ms\n",  diff / 1000.0);

	return;
}
