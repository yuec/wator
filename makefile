#wator: sim.c shark.c fish.c wator.c
#	gcc -o wator -ggdb -O0 -std=c99 -Wall -fopenmp sim.c shark.c fish.c wator.c -lm
#
#clean:
#	rm wator

GCC=gcc
DBGFLAG=-ggdb -Wall
LIBS=-lm
OMP=-fopenmp
FLAG=-O0

all: wator

wator: fish.o shark.o sim.o wator.o utils.o
	$(GCC) -o wator fish.o shark.o sim.o wator.o utils.o $(DBGFLAG) $(LIBS) \
-std=c99  $(OMP) $(FLAG)

utils.o: utils.c utils.h
	$(GCC) -c utils.c $(DBGFLAG) -std=c99 $(FLAG)

fish.o: fish.c fish.h
	$(GCC) -c fish.c $(DBGFLAG) -std=c99 $(OMP) $(FLAG)

shark.o: shark.c shark.h
	$(GCC) -c shark.c $(DBGFLAG) -std=c99 $(OMP) $(FLAG)

wator.o: wator.c wator.h
	$(GCC) -c wator.c $(DBGFLAG) -std=c99 fish.o shark.o $(OMP) $(FLAG) $(LIBS)

sim.o: sim.c 
	$(GCC) -c sim.c $(DBGFLAG) -std=c99 fish.o shark.o $(OMP) $(FLAG) $(LIBS)

clean:
	rm -f *.o wator
